<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://nip.cetin.cz/b2b/CetinIntegrationMessage/1.0" targetNamespace="http://nip.cetin.cz/b2b/CetinIntegrationMessage/1.0" elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xs:annotation>
		<xs:documentation>A template all integration messages will be derived from.</xs:documentation>
	</xs:annotation>
	<xs:complexType name="RequestMessage">
		<xs:annotation>
			<xs:documentation>Template (extension base) for request message.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="requestHeader" type="RequestHeader">
				<xs:annotation>
					<xs:documentation>Request header.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ResponseMessage">
		<xs:annotation>
			<xs:documentation>Template (extension base) for response message.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="responseHeader" type="ResponseHeader">
				<xs:annotation>
					<xs:documentation>Response header.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:annotation>
		<xs:documentation>Message headers (common types).</xs:documentation>
	</xs:annotation>
	<xs:complexType name="Header" abstract="true">
		<xs:annotation>
			<xs:documentation>Common abstract header.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="messageId" type="xs:string">
				<xs:annotation>
					<xs:documentation>Unique message identifier.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="timestamp" type="xs:dateTime">
				<xs:annotation>
					<xs:documentation>Message creation timestamp.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="priority" type="xs:int" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Message priority (provider may ignore this information)</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="correlationId" type="xs:string" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Optional identifier used for mapping request and response messages.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="trackingInfo" type="TrackingInfo" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Business specific information which might be used for tracking purposes (e.g. monitoring, logging, SLA, etc.).</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="properties" type="Properties" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Additional context information (name/value)</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="Properties">
		<xs:sequence>
			<xs:element name="property" type="Property" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="Property">
		<xs:sequence>
			<xs:element name="name" type="xs:string">
				<xs:annotation>
					<xs:documentation>Property name</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="value" type="xs:string" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Property value</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="RequestHeader">
		<xs:annotation>
			<xs:documentation>Common header of request messages.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="Header">
				<xs:sequence>
					<xs:element name="consumerIdentification" type="xs:string">
						<xs:annotation>
							<xs:documentation>Identification of a consumer of the service (the request sending).</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="providerIdentification" type="xs:string" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Constant identification of provider. Identification of a provider of the service (the response sending/responding application).</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="ResponseHeader">
		<xs:annotation>
			<xs:documentation>Common header of response messages.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="Header">
				<xs:sequence>
					<xs:element name="consumerIdentification" type="xs:string" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Identification of a consumer of the service (the request sending).</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="providerIdentification" type="xs:string">
						<xs:annotation>
							<xs:documentation>Constant identification of provider. Identification of a provider of the service (the response sending/responding application).</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:annotation>
		<xs:documentation>Supporting common types.</xs:documentation>
	</xs:annotation>
	<xs:complexType name="TrackingInfo">
		<xs:annotation>
			<xs:documentation>Business specific information which might be used for tracking purposes (e.g. monitoring, logging, SLA, etc.).</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="businessId" type="TrackingIInfoDetail" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Identifier mapping the message to a business entity (e.g. MSISDN, CustomerId, etc.).</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="conversationId" type="TrackingIInfoDetail" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Identifier mapping the message to a business process (such as "Create Order"), should be propagated through all requests connected with the business process.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="userId" type="TrackingIInfoDetail" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Identification of a user or an application (significant for security audit purpose).</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="TrackingIInfoDetail">
		<xs:annotation>
			<xs:documentation>Tracking info identifier detail</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="value" type="xs:string">
				<xs:annotation>
					<xs:documentation>Identifier value</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="meaning" type="xs:string" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Identifier meaning (specific service parameter name)</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:annotation>
		<xs:documentation>Error response message.</xs:documentation>
	</xs:annotation>
	<xs:complexType name="ErrorResponseMessageBody">
		<xs:annotation>
			<xs:documentation>Error detail to be contained within error response message.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="errorCode" type="xs:string">
				<xs:annotation>
					<xs:documentation>A well-defined error code</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="errorMessage" type="xs:string" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Optional text message describing the error.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ErrorResponseMessage">
		<xs:annotation>
			<xs:documentation>Error response message.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="ResponseMessage">
				<xs:sequence>
					<xs:element name="responseBody" type="ErrorResponseMessageBody">
						<xs:annotation>
							<xs:documentation>Response body.</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="errorResponse" type="ErrorResponseMessage">
		<xs:annotation>
			<xs:documentation>Error response message. It is generated by the integration layer in a case there is a problem to deliver request to the service provider due to infrastructure problem (e.g. communication with system fails, database error).</xs:documentation>
		</xs:annotation>
	</xs:element>
	<xs:complexType name="NotificationTrackingInfo">
		<xs:sequence>
			<xs:element name="operator" type="xs:string">
				<xs:annotation>
					<xs:documentation>Identification of virtual operator. For TEF use strictly value "TEF.CZ"</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="channel" type="xs:string">
				<xs:annotation>
					<xs:documentation>Channel of notifications</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="eventGroup" type="xs:string">
				<xs:annotation>
					<xs:documentation>Event Group</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="eventType" type="xs:string">
				<xs:annotation>
					<xs:documentation>Type of event</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="eventSubtype" type="xs:string" minOccurs="0">
				<xs:annotation>
					<xs:documentation>For case of more specific events under main category of event (eventType).</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="eventEntity" type="xs:string">
				<xs:annotation>
					<xs:documentation>Entity on which the event is raised</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="eventEntityValue" type="xs:string">
				<xs:annotation>
					<xs:documentation>Id(value) of concrete entity</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="notificationDate" type="xs:dateTime">
				<xs:annotation>
					<xs:documentation>Date when notification was created/raised by source system.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="eventDate" type="xs:dateTime" minOccurs="0">
				<xs:annotation>
					<xs:documentation>When the event occured in the source system (e.g. process date in system).</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="eventExpirationDate" type="xs:dateTime" minOccurs="0">
				<xs:annotation>
					<xs:documentation>When the notification should be handled by target consumers as expired. When not provided, then notification has not defined expiration (never expires).</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="sequenceNumber" type="xs:long" minOccurs="0">
				<xs:annotation>
					<xs:documentation>The incresing seq. number generated by source system - in the case there is intent to distinguish notifications in sequence by target systems.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="NotificationRequestHeader">
		<xs:complexContent>
			<xs:extension base="RequestHeader">
				<xs:sequence>
					<xs:element name="notificationTrackingInfo" type="NotificationTrackingInfo"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="NotificationRequestMessage">
		<xs:sequence>
			<xs:element name="requestHeader" type="NotificationRequestHeader"/>
		</xs:sequence>
	</xs:complexType>
</xs:schema>
