:path: fragments/
:imagesdir: images/

ifdef::rootpath[]
:imagesdir: {rootpath}{path}{imagesdir}
endif::rootpath[]

ifndef::rootpath[]
:rootpath: ./../../
endif::rootpath[]

= Performance

*Limity rozhraní*

X-RateLimit-Limit → 1000

X-BurstLimit-Limit → 500

*V response HTTP hlavičkách je vracen stav čerpání limitů + informace o
resetu.*

X-RateLimit-Reset → 1425967200

X-BurstLimit-Remaining → 498

X-RateLimit-Remaining → 996

V případě překročení limitu vrací docusign HTTP 207

Maximální velikost zprávy (request/response): 70MB
