= activate()

[plantuml, activate, png]
----
@startuml
  "Service Consumer" -> "FisOrderManagement/1.1": «SOAP-MTOM/HTTPS» activate(ActivateRequest)
activate "FisOrderManagement/1.1"

  "FisOrderManagement/1.1" -> "FisOrderManagement/1.1.1": 
activate "FisOrderManagement/1.1.1"

  "FisOrderManagement/1.1.1" -> "FisOrderManagement/1.1": «SOAP-MTOM/HTTPS» activate(ActivateRequest)
activate "FisOrderManagement/1.1"

  "FisOrderManagement/1.1" --> "FisOrderManagement/1.1.1": 
deactivate "FisOrderManagement/1.1"

  "FisOrderManagement/1.1.1" --> "FisOrderManagement/1.1": 
deactivate "FisOrderManagement/1.1.1"

  "FisOrderManagement/1.1" --> "Service Consumer": 
deactivate "FisOrderManagement/1.1"

@enduml
----
