= Sequence Diagrams

include::SequenceDiagram_acknowledge.adoc[leveloffset=+1]
include::SequenceDiagram_activate.adoc[leveloffset=+1]
include::SequenceDiagram_cancel.adoc[leveloffset=+1]
include::SequenceDiagram_commit.adoc[leveloffset=+1]
include::SequenceDiagram_create.adoc[leveloffset=+1]
include::SequenceDiagram_getOrder.adoc[leveloffset=+1]
