= cancel()

[plantuml, cancel, png]
----
@startuml
  "Service Consumer" -> "FisOrderManagement/1.0": «SOAP-MTOM/HTTPS» cancel(CancelRequest)
activate "FisOrderManagement/1.0"

  "FisOrderManagement/1.0" -> "FisOrderManagement/1.0.1": 
activate "FisOrderManagement/1.0.1"

  "FisOrderManagement/1.0.1" -> "FisOrderManagement/1.0": «SOAP-MTOM/HTTPS» cancel(CancelRequest)
activate "FisOrderManagement/1.0"

  "FisOrderManagement/1.0" --> "FisOrderManagement/1.0.1": 
deactivate "FisOrderManagement/1.0"

  "FisOrderManagement/1.0.1" --> "FisOrderManagement/1.0": 
deactivate "FisOrderManagement/1.0.1"

  "FisOrderManagement/1.0" --> "Service Consumer": 
deactivate "FisOrderManagement/1.0"

@enduml
----
